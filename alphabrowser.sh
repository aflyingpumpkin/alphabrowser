#!/bin/bash

sleepinterval="0.1" #How often the script will check for a change in the browsers position.
browserclass="Vivaldi" #The class of the browser that the script is meant for, can be found using `xprop | grep WM_CLASS`
while true
do
	xdotool search --class "$browserclass" getwindowgeometry | sed -n -e '2p' | sed -e 's/Position://g' -e 's/\s//g' -e 's/(screen:.)//g' -e 's/.*,//g' > /tmp/browserposy
	xdotool search --class "$browserclass" getwindowgeometry | sed -n -e '2p' | sed -e 's/Position://g' -e 's/\s//g' -e 's/(screen:.)//g' -e 's/,.*//g' > /tmp/browserposx
	sleep sleepinterval
done
