`alphabrowser` is a project with the goal of adding in alpha support to web browsers.

Currently, this is done through three different files:
1. A css file which just set's the background to the desired image, and applies a color atop it.
2. A bash file to detect where the browser is located on screen.
3. A javascript file meant to be used with the tampermonkey browser extension, which detects the output of the bash script and will adjust the backgrounds offset accordingly. This is file also takes in parameters to account for UI elements in the browser.

**Tested Platforms:**

1. `Vivaldi 4.3.2439.44 stable`
2. `dwm` (my personal build)

**Known Issues:**

1. UI elements must be manually inputted by the user, meaning the script is intolerant to if UI elements change on the fly.
2. The image offset will only update on page refresh or a new page being opened. Meaning that if the location of the browser is changed, the page must be refreshed for the background to stay in sync.
3. It is only emulating transparency with css `background-image` and therefor will not use the compositor. This will likely never get fixed due to it likely requiring the browser itself being forked.
4. On some sites the image does not appear at all, it is still unclear as to why.
5. It cannot read the background from a local file.
6. Does not work in floating mode on `dwm`, this appears to be an issue with `xdotool` though I may be wrong.

**Screenshot(s)**

![Screenshot 1](/Screenshot1.png)
