// ==UserScript==
// @name         alphabrowser
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Tampermonkey script to make make alphabrowser work.
// @author       aflyingpumpkin
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @match        *://*/*
// @resource     posy file:///tmp/browserposy
// @resource     posx file:///tmp/browserposx
// ==/UserScript==
var xOffset = 2;
var yOffset = 34;
(function() {
    var px = Number(GM_getResourceText("posx")) + xOffset;
    var py = Number(GM_getResourceText("posy")) + yOffset;
    console.log ("X:", px,"px", "Y:", py,"px");
    document.documentElement.style.setProperty('--x', px);
    document.documentElement.style.setProperty('--y', py);
})();
